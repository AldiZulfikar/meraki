<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class forum extends Model
{
    use HasFactory;

    protected $table = 'forums';

    protected $fillable = [
        'judul', 'slug', 'konten', 'user_id'
    ];

    protected $hidden = [];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function komentar()
    {
        return $this->hasMany(komentarForum::class);
    }

}
