<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class adopsi extends Model
{
    use HasFactory;

    protected $table = 'adopsis';

    protected $fillable = [
        'user_id', 'judul', 'slug',  'alamat', 'ras', 'konten', 'gambar_binatang', 'persyaratan'
    ];

    protected $hidden = [];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
