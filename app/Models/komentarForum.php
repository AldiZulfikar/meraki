<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class komentarForum extends Model
{
    use HasFactory;

    protected $table = 'komentar_forums';

    protected $fillable = [
        'user_id', 'forum_id', 'konten'
    ];

    protected $hidden = [];

    public function komentar()
    {
        return $this->belongsTo(forum::class, 'forum_id', 'id');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
