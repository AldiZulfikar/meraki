<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\kategori;

class artikel extends Model
{
    use HasFactory;
    protected $table = 'artikels';

    protected $fillable = [
        'judul', 'slug', 'konten', 'kategori_id', 'user_id', 'gambar_artikel', 'gambar_artikel1', 'gambar_artikel2', 'is_active', 'views'
    ];

    protected $hidden = [];

    public function kategori()
    {
        return $this->belongsTo(kategori::class, 'kategori_id', 'id');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
