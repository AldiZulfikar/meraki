<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Models\artikel;
use App\Models\User;
use App\Models\kategori;
use Illuminate\Support\Facades\Storage;


class artikelController extends Controller
{
    public function index()
    {
        $artikel = artikel::all();

        return view("admin.artikel.index", [
            'artikel' => $artikel
        ]);
    }

    public function add(){
        $artikel = artikel::all();
        $kategori = kategori::all();

        return view("admin.artikel.add", [
            'artikel' => $artikel,
            'kategori' => $kategori
        ]);
    }

    public function create(Request $request){
        // validasi inputannya udah sesuai belum
        $this->validate($request, [
            'judul' => 'required',
            'gambar_artikel' => 'required|mimes:jpg,jpeg,bmp,png|max:5012',
            'kategori_id' => 'required',
            'konten' => 'required',
        ]);

        $data = $request->all();
        $data['slug'] = Str::slug($request->judul);
        $data['user_id'] = Auth::id();
        $data['views'] = 0;
        $data['gambar_artikel'] = $request->file('gambar_artikel')->store('artikels');
        $data['gambar_artikel1'] = $request->file('gambar_artikel1')->store('artikels');
        $data['gambar_artikel2'] = $request->file('gambar_artikel2')->store('artikels');

        artikel::create($data);
        
        // redirect ke halaman index artikel
        return redirect()->route('admin-artikel')->with('message','success');
    }

    public function edit($id){
        $artikel = artikel::find($id);
        $kategori = kategori::all();

        return view("admin.artikel.edit", [
            'artikel' => $artikel,
            'kategori' => $kategori
        ]);
    }

    public function update(Request $request, $id){

        if(empty($request->file('gambar_artikel'))){
            $artikel = artikel::find($id);
            $artikel->update([
                'judul' => $request->judul,
                'konten' => $request->konten,
                'slug' => Str::slug($request->judul),
                'kategori_id' => $request->kategori_id,
                'user_id' => Auth::id(),
                'is_active' => $request->is_active,  
            ]);
            // redirect ke halaman index artikel
            return redirect()->route('admin-artikel')->with('message','success');
        }else{
            $artikel = artikel::find($id);
            Storage::delete($artikel->gambar_artikel);
            Storage::delete($artikel->gambar_artikel1);
            Storage::delete($artikel->gambar_artikel2);
            $artikel->update([
                'judul' => $request->judul,
                'konten' => $request->konten,
                'slug' => Str::slug($request->judul),
                'kategori_id' => $request->kategori_id,
                'user_id' => Auth::id(),
                'is_active' => $request->is_active,  
                'gambar_artikel' => $request->file('gambar_artikel')->store('artikels'),
                'gambar_artikel1' => $request->file('gambar_artikel1')->store('artikels'),
                'gambar_artikel2' => $request->file('gambar_artikel2')->store('artikels'),
            ]);
            // redirect ke halaman index artikel
            return redirect()->route('admin-artikel')->with('message','success');
        }   
    }

    public function delete($id){
        $artikel = artikel::find($id);
        Storage::delete($artikel->gambar_artikel);
        Storage::delete($artikel->gambar_artikel1);
        Storage::delete($artikel->gambar_artikel2);

        // delete data by the id
        $artikel->delete($id);

        // redirect ke halaman index ss
        return redirect()->route('admin-artikel')->with('message','Success');
    }
}
