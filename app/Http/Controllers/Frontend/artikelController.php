<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\artikel;

class artikelController extends Controller
{
    public function index($slug){
        $artikel = artikel::where('slug', $slug)->first();

        return view("pages.artikel", [
            'artikel' => $artikel
        ]);
    }
}
