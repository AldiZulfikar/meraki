<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\forum;
use App\Models\komentarForum;
use Illuminate\Support\Str;

class forumController extends Controller
{
    public function index()
    {
        $forum = forum::paginate(10);
        return view('pages.Forum.index', compact(['forum']));
    }

    public function add(){
        return view('pages.Forum.add');
    }

    public function create(Request $request){
        // validasi inputannya udah sesuai belum
        $this->validate($request, [
            'judul' => 'required',
            'konten' => 'required',
        ]);

        $data = $request->all();
        $data['slug'] = Str::slug($request->judul);
        $data['user_id'] = Auth::id();

        forum::create($data);
        
        return redirect()->route('forum-diskusi');
    }

    public function detail($slug){
        $forum = forum::where('slug', $slug)->first();
        $komentar = komentarForum::all();
        return view("pages.Forum.detail", [
            'forum' => $forum,
            'komentar' => $komentar,
        ]);
    }

    public function post(Request $request){
        $this->validate($request, [
            'konten' => 'required'
        ]);

        $data = $request->all();
        $data['user_id'] = Auth::id();

        komentarForum::create($data);
        
        return redirect()->back();
    }
}
