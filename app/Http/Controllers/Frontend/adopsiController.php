<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\adopsi;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Models\User;

class adopsiController extends Controller
{
    public function index()
    {
        $adopsi = adopsi::all();

        return view("pages.Adopsi.adopsi", [
            'adopsi' => $adopsi
        ]);
    }

    public function add(){
        $adopsi = adopsi::all();

        return view("pages.Adopsi.add", [
            'adopsi' => $adopsi,
        ]);
    }

    public function create(Request $request){
        // validasi inputannya udah sesuai belum
        $this->validate($request, [
            'judul' => 'required',
            'gambar_binatang' => 'required|mimes:jpg,jpeg,bmp,png|max:5012',
            'persyaratan' => 'required|mimes:pdf|max:5012',
            'ras' => 'required',
            'konten' => 'required',
        ]);

        $data = $request->all();
        $data['slug'] = Str::slug($request->judul);
        $data['user_id'] = Auth::id();
        $data['gambar_binatang'] = $request->file('gambar_binatang')->store('adopsi_gambar');
        $data['persyaratan'] = $request->file('persyaratan')->store('adopsi_pdf');

        adopsi::create($data);
        
        // redirect ke halaman index adopsi
        return redirect()->route('pages.adopsi')->with('message','success');
    }

    public function detail($slug){
        $adopsi = adopsi::where('slug', $slug)->first();
        return view("pages.Adopsi.detail", [
            'adopsi' => $adopsi,
        ]);
    }
}
