<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\artikel;

class homepageController extends Controller
{
    public function index()
    {
        $artikel = artikel::paginate(3);
        return view("pages.homepage", [
            'artikel' => $artikel
        ]);
    }
}
