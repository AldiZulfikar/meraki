<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', [App\Http\Controllers\Frontend\homepageController::class, 'index'])->name('pages.homepage');

Route::get('auth/google', [App\Http\Controllers\googleController::class, 'redirectToGoogle'])->name('google.login');

Route::get('auth/google/callback', [App\Http\Controllers\googleController::class, 'handleGoogleCallback'])->name('google.callback');

Route::get('/baca-artikel/{slug}', [App\Http\Controllers\Frontend\artikelController::class, 'index'])->name('baca-artikel');

Route::get('/tranding', [App\Http\Controllers\Frontend\trandingController::class, 'index'])->name('baca-artikel');

Route::get('/helodoc', [App\Http\Controllers\Frontend\helodocController::class, 'index'])->name('pages.helodoc');

Route::get('/adopsi', [App\Http\Controllers\Frontend\adopsiController::class, 'index'])->name('pages.adopsi');

Route::get('/forum', [App\Http\Controllers\Frontend\forumController::class, 'index'])->name('forum-diskusi');

Route::get('/salon', [App\Http\Controllers\Frontend\salonController::class, 'index'])->name('salon-hewan');

Route::get('/donasi', [App\Http\Controllers\Frontend\donasiController::class, 'index'])->name('pages.donasi');

Route::get('/AboutUs', [App\Http\Controllers\Frontend\aboutUsController::class, 'index'])->name('pages.aboutUs');

# FRONTEND HARUS LOGIN
Route::group(['middleware' => 'auth'], function () {
    Route::group(['middleware' => 'user'], function () {
        Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

        Route::get('/adopsi/add', [App\Http\Controllers\Frontend\adopsiController::class, 'add'])->name('adopsi-buat');
        Route::post('/adopsi', [App\Http\Controllers\Frontend\adopsiController::class, 'create'])->name('buat-adopsi');
        Route::get('/adopsi/detail/{slug}', [App\Http\Controllers\Frontend\adopsiController::class, 'detail'])->name('adopsi-detail');

        Route::get('/forum/add', [App\Http\Controllers\Frontend\forumController::class, 'add'])->name('forum-diskusi-buat');
        Route::post('/forum', [App\Http\Controllers\Frontend\forumController::class, 'create'])->name('forum-buat-diskusi');
        Route::get('/forum/detail/{slug}', [App\Http\Controllers\Frontend\forumController::class, 'detail'])->name('forum-lihat-diskusi');
        Route::post('/forum', [App\Http\Controllers\Frontend\forumController::class, 'post'])->name('forum-komen-diskusi');
    });
});

Route::group(['middleware' => 'auth'], function () {
    Route::group(['middleware' => 'admin'], function () {
        Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
        # HALAMAN ADMIN
        Route::get('aofhf', [App\Http\Controllers\Admin\AdminController::class, 'index'])->name('admin-dashboard');

        // HALAMAN ARTIKEL ADMIN
        Route::get('aofhf/artikel', [App\Http\Controllers\Admin\artikelController::class, 'index'])->name('admin-artikel');
        Route::get('aofhf/artikel/add', [App\Http\Controllers\Admin\artikelController::class, 'add'])->name('admin-tambah-artikel');
        Route::post('aofhf/artikel', [App\Http\Controllers\Admin\artikelController::class, 'create'])->name('admin-artikel-tambah');
        Route::get('aofhf/artikel/{id}', [App\Http\Controllers\Admin\artikelController::class, 'edit'])->name('admin-artikel-update');
        Route::put('aofhf/artikel/{id}', [App\Http\Controllers\Admin\artikelController::class, 'update'])->name('admin-update-artikel');
        Route::delete('aofhf/artikel/{id}', [App\Http\Controllers\Admin\artikelController::class, 'delete'])->name('admin-artikel-delete');

    });
});