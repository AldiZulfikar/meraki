<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMitrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mitras', function (Blueprint $table) {
            $table->id();
            $table->string('nama_pemilik');
            $table->string('nama_tempat');
            $table->string('jenis');
            $table->string('slug');
            $table->string('alamat');
            $table->string('no_wa');
            $table->string('chat_wa');
            $table->string('layanan1'); //Perawatan Hewan, Studio Foto Hewan, antar jemput, Lainnya
            $table->string('layanan2');
            $table->string('layanan3');
            $table->string('layanan4');
            $table->float('harga1');
            $table->float('harga2');
            $table->string('waktu_buka');
            $table->string('kd_transaksi');
            $table->string('review');
            $table->text('konten');
            $table->date('masa_aktiv');
            $table->string('gambar_tempat');
            $table->string('ktp'); //ktp, sertifikat buka klinik, 
            $table->string('surat_izin');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mitras');
    }
}
