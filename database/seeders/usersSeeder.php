<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class usersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrator',
            'jenis_kelamin' => 'perempuan',
            'notel' => '088967102',
            'profil' => 'ola.png',
            'role' => 'admin',
            'email' => 'merakitech.office@gmail.com',
            'google_id' => '123123123',
            'password' => Hash::make('cuanterus'),
        ]);
        DB::table('users')->insert([
            'name' => 'Meraki Tech',
            'jenis_kelamin' => 'perempuan',
            'notel' => '088967102',
            'profil' => 'ola.png',
            'role' => 'user',
            'email' => 'user@gmail.com',
            'google_id' => '123123123',
            'password' => Hash::make('cuanterus'),
        ]);
    }
}
