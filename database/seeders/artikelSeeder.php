<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class artikelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artikels')->insert([
            'judul' => 'Cara Merawat Kucing Sakit',
            'slug' => 'cara-merawat-kucing-sakit',
            'konten' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum obcaecati doloribus necessitatibus harum. Pariatur excepturi harum quidem distinctio molestias vero, enim vel ipsa labore amet dolore corrupti quaerat est omnis.',
            'kategori_id' => 1,
            'user_id' => 1,
            'gambar_artikel' => 'kucingsakit.png',
            'gambar_artikel1' => 'kucingsakit.png',
            'gambar_artikel2' => 'kucingsakit.png',
            'is_active' => true,
            'views' => 130,
        ]);
    }
}
