<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class forumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forums')->insert([
            'judul' => 'Bulu si meng rontok mendadak',
            'slug' => 'bulu-si-meng-rontok-mendadak',
            'konten' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatum provident id voluptates, vel sed impedit expedita rem quasi esse quae. Obcaecati minima officiis adipisci eaque labore praesentium fugiat nihil consequuntur.',
            'user_id' => 1
        ]);
    }
}
