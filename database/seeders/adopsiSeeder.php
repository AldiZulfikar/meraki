<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class adopsiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('adopsis')->insert([
            'user_id' => 1,
            'judul' => 'Adopsi Kucing Oren',
            'slug' => 'adopsi-kucing-oren',
            'alamat' => 'Indonesia, Bumi, Galaxy Bima Sakti',
            'ras' => 'Anggora',
            'konten' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum obcaecati doloribus necessitatibus harum. Pariatur excepturi harum quidem distinctio molestias vero, enim vel ipsa labore amet dolore corrupti quaerat est omnis.',
            'gambar_binatang' => 'kucingsakit.png',
            'persyaratan' => 'kucingsakit.pdf',
        ]);
    }
}
