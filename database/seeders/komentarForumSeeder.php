<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class komentarForumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('komentar_forums')->insert([
            'user_id' => 1,
            'forum_id' => 1,
            'konten' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatum provident id voluptates, vel sed impedit expedita rem quasi esse quae. Obcaecati minima officiis adipisci eaque labore praesentium fugiat nihil consequuntur.'
        ]);
    }
}
