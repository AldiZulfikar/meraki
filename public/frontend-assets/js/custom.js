var WelcomeSwiper = new Swiper(".welcome-swiper", {
    loop: true,
    speed: 1000,

    lazy: true,
    pagination: {
        el: ".welcome-swiper .swiper-pagination",
        clickable: true,
    },
    autoplay: {
        delay: 3000,
        disableOnInteraction: true,
    },
});
var ArtikelSwiper = new Swiper(".artikel-swiper", {
    loop: true,
    speed: 1000,

    lazy: true,
    pagination: {
        el: ".artikel-swiper .swiper-pagination",
        clickable: true,
    },
    autoplay: {
        delay: 2500,
        disableOnInteraction: true,
    },
});

$('.humber').click(function() {
    $('.sidebar__section-1').addClass('active')
    setTimeout(function(){
        $('.sidebar__section-2').addClass('active')
    },1500);
  });
$('.close').click(function() {
    $('.sidebar__section-1').removeClass('active')
    setTimeout(function(){
        $('.sidebar__section-2').removeClass('active')
    },100);
  });
$('.btn-komentar').click(function() {
    $('#comment').addClass('active')

  });
