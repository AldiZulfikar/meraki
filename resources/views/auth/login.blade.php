@extends('layouts.app')
@section('content')
<div class="login">
    <div class="login__wrap row justify-content-center">
        <div class="login__nav" >
            <div class="img nav-sigin active">
                <img class="btn-dekstop" src="frontend-assets/image/icon/button.png" alt="">
                <p class="btn nav-sigin active">Sign In</button>
            </div>
            <div class="img nav-signup">
                <img src="frontend-assets/image/icon/button.png" alt="">
                <p class="btn nav-signup">Sign Up</button>
                </div>
        </div>
                <div class="login__hero col-md-5 col-sm-12 col-lg-5">
                    <div class="ornamen"></div>
                    <div class="ornamen2">
                    </div>
                    <div class="welcome-swiper swiper-container col-md-10">
                        <h3 class="text-white text-center">Get Hero</h3>
                        <div class="swiper-wrapper">
                           <div class="swiper-slide">
                              <div class="image" style="background-image: url('frontend-assets/image/viktor/slide1.png');">
                            </div>
                              <p class="text-white text-center mt-1">kita membantu Lorem ipsum dolor sit amet</p>
                           </div>
                           <div class="swiper-slide">
                              <div class="image" style="background-image: url('frontend-assets/image/viktor/slide2.png');"></div>
                              <p class="text-white text-center mt-1">kita sangat ingin membantu Lorem ipsum do</p>

                            </div>
                           <div class="swiper-slide">
                              <div class="image" style="background-image: url('frontend-assets/image/viktor/slide3.png');"></div>
                              <p class="text-white text-center mt-1">Terutama membantu Lorem ipsum dolor sit amet</p>

                            </div>
                        </div>
                        <div class="swiper-pagination"></div>
                     
                     </div>
                </div>
                <div class="login__body col-md-12 col-sm-12 col-lg-7 mt-n5 ">
                    <div class="login__container container-signin active">
                        <div class="login__logo">
                            <img src="../../frontend-assets/image/logo/logo.png" alt="">
                        </div>
                        <div class="login__title text-center mt-3">
                            <h3>Walcome Back !</h3>
                            <p>Log in to your existant account </p>
                        </div>
                        <form method="POST" action="{{ route('login') }}" class="col-sm-8 col-lg-12 col-md-12">            
                            @csrf
                            <div class="form-group row mt-4">
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required placeholder=" {{ ('Enter Your Email') }}" autofocus>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required required placeholder=" {{ ('Enter Your Password') }}" autocomplete="current-password">
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
    
                            <div class="form-group row mb-0 ">
                                <div class="col-md-6 offset-md-4 login__btn">   
                                    @if (Route::has('password.request'))
                                    <a class="fotget btn btn-link p-0" href="{{ route('password.request') }}">
                                        {{ __('Forgot Password?') }}
                                    </a>
                                    @endif
    
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="login__container  container-signup ">
                        <div class="login__logo">
                            <img src="../../frontend-assets/image/logo/logo.png" alt="">
                        </div>
                        <div class="login__title text-center mt-3">
                            <h3>Let’s Get Started! </h3>
                            <p>Create  an account to join our journey </p>
                        </div>
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
    
                            <div class="form-group row">    
                                <div class="col-md-6">
                                    <input id="name" type="text" placeholder="Your Name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
    
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    
                            <div class="form-group row">    
                                <div class="col-md-6">
                                    <input id="email" type="email" placeholder="Your Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
    
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input id="password" placeholder="Your Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
    
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input id="password-confirm" placeholder="Confrim Password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>
    
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4 login__btn regis-btn">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="login__footer">
                        <p class="pr-5 mt-3">Or Login with</p>
                        <a class="btn btn-link" href="{{ route('google.login') }}">
                            {{ __('Login dengan Google') }}
                         </a>
                    </div>
                </div>
    </div>
</div>
@endsection

