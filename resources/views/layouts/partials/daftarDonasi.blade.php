<section class="donasi__daftar d-flex align-items-center justify-content-between">
  <div class="box-donasi d-flex align-items-center justify-content-center flex-column col-12">
    <div class="container  ">
        {{-- nanti kamu looping ya datanya --}}
        <div class="row  mb-5">
            <div class="col-12 col-sm-12 col-lg-4 col-md-6 mt-3">  
               <div class="card " style="width: 18rem;">
                <img class="card-img-top"  src="{{ asset('frontend-assets/image/foto/donasi.png') }}" alt="">
                <div class="card-body">
                  {{-- nanti stylenya prograsbarnya yg widthnya kamu atur sesuai dana yg terkumpul ya --}}
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <h5 class="card-title">Bantu operasi kaki</h5>
                  <p class="card-text">Kurang Rp 200.000  </p>
                  <p class="card-text mt-n3">3 hari lagi</p>
                  <a href="#" class="btn btn-primary">Donasi</a>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-12 col-lg-4 col-md-6 mt-3 ">  
                <div class="card" style="width: 18rem;">
                <img class="card-img-top"  src="{{ asset('frontend-assets/image/foto/donasi.png') }}" alt="">
                <div class="card-body">
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <h5 class="card-title">Bantu operasi kaki</h5>
                  <p class="card-text">Kurang Rp 200.000  </p>
                  <p class="card-text mt-n3">3 hari lagi</p>
                  <a href="#" class="btn btn-primary">Donasi</a>
                </div>
              </div></div>
            <div class="col-12 col-sm-12 col-lg-4 col-md-6 mt-3">  
                <div class="card" style="width: 18rem;">
                <img class="card-img-top"  src="{{ asset('frontend-assets/image/foto/donasi.png') }}" alt="">
                <div class="card-body">
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  <h5 class="card-title">Bantu operasi kaki</h5>
                  <p class="card-text">Kurang Rp 200.000  </p>
                  <p class="card-text mt-n3">3 hari lagi</p>
                  <a href="#" class="btn btn-primary">Donasi</a>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-12 col-lg-4 col-md-6 mt-3">  
              <div class="card" style="width: 18rem;">
              <img class="card-img-top"  src="{{ asset('frontend-assets/image/foto/donasi.png') }}" alt="">
              <div class="card-body">
                <div class="progress">
                  <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h5 class="card-title">Card title</h5>  
                <p class="card-text">Kurang Rp 200.000  </p>
                <p class="card-text mt-n3">3 hari lagi</p>
                <a href="#" class="btn btn-primary">Donasi</a>
              </div>
            </div></div>
          <div class="col-12 col-sm-12 col-lg-4 col-md-6 mt-3">  
              <div class="card" style="width: 18rem;">
              <img class="card-img-top"  src="{{ asset('frontend-assets/image/foto/donasi.png') }}" alt="">
              <div class="card-body">
                <div class="progress">
                  <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Kurang Rp 200.000  </p>
                <p class="card-text mt-n3">3 hari lagi</p>
                <a href="#" class="btn btn-primary">Donasi</a>
              </div>
            </div></div>
          <div class="col-12 col-sm-12 col-lg-4 col-md-6 mt-3">  
              <div class="card" style="width: 18rem;">
              <img class="card-img-top"  src="{{ asset('frontend-assets/image/foto/donasi.png') }}" alt="">
              <div class="card-body">
                <div class="progress">
                  <div class="progress-bar" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Kurang Rp 200.000  </p>
                <p class="card-text mt-n3">3 hari lagi</p>
                <a href="#" class="btn btn-primary">Donasi</a>
              </div>
            </div>
      </div>
          </div>

          
    </div>
  </div>
  
</section>