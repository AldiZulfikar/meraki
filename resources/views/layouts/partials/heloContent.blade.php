<section class="hello__content  d-flex align-items-center">
    <div class="container">
        <div class="row content-1 d-flex align-items-center">
            <div class="desc">
                <h1 class="text-center pb-5 mb-5">Chat dengan dokter terpercaya</h1>
               <div class="col-10 d-flex mx-auto mt-5">
                   <div class="col-6 col-sm-12 col-lg-6 col-md-6">
                       <div class="img-helloContent">
                           <img class="position-absolute" src="{{ asset('frontend-assets/image/viktor/hello3.png') }}" alt="">
                       </div>
                   </div>
                   <div class="col-6 col-sm-12 col-lg-6 col-md-6 text-end">
                       <h1>Pilih dari puluhan dokter berpengalaman dan chat online sekarang</h1>
                   </div>
               </div>
           </div>
       </div>
       <div class="row content-1 d-flex align-items-center">
           <div class="desc">
               <div class="col-10 d-flex mx-auto mt-5">
                <div class="col-6 col-sm-12 col-lg-6 col-md-6 text-start">
                    <h1>Pilih dari puluhan dokter berpengalaman dan chat online sekarang</h1>
                </div>
                   <div class="col-6 col-sm-12 col-lg-6 col-md-6">
                       <div class="img-helloContent">
                           <img class="position-absolute" src="{{ asset('frontend-assets/image/viktor/hello4.png') }}" alt="">
                       </div>
                   </div>
                   
               </div>
           </div>
       </div>
       <div class="row content-1 d-flex align-items-center">
           <div class="desc">
               <div class="col-10 d-flex mx-auto mt-5">
             
                   <div class="col-6 col-sm-12 col-lg-6 col-md-6">
                       <div class="img-helloContent">
                           <img class="position-absolute" src="{{ asset('frontend-assets/image/viktor/hello5.png') }}" alt="">
                       </div>
                   </div>
                   <div class="col-6 col-sm-12 col-lg-6 col-md-6 text-end">
                    <h1>Pilih dari puluhan dokter berpengalaman dan chat online sekarang</h1>
                </div>
               </div>
           </div>
       </div>
       <div class="row content-1 d-flex align-items-center">
           <div class="desc">
               <h1 class="text-center">
                   Kami Pastikan
               </h1>
               <div class="col-10 d-flex mx-auto mt-5 text-center " >
                   <div class="col-4 col-sm-12 col-lg-4 col-md-4">
                    <div class="card border-0" style="width: 18rem; ">
                        <img class="card-img-top" src="{{ asset('frontend-assets/image/viktor/v1.png') }}" alt="">
                        <div class="card-body">
                          <p class="card-text">Chat dengan Dokter berpengalaman dan terverifikasi dengan beragam pilihan dokter umum dan dokter spesialis di seluruh Indonesia</p>
                        </div>
                      </div>
                   </div>
                   <div class="col-4 col-sm-12 col-lg-4 col-md-4">
                    <div class="card border-0" style="width: 18rem;">
                        <img class="card-img-top" src="{{ asset('frontend-assets/image/viktor/v2.png') }}" alt="">
                        <div class="card-body">
                          <p class="card-text">Dapatkan penjelasan dan saran medis yang akurat dan dapat dipercaya</p>
                        </div>
                      </div>
                   </div>
                   <div class="col-4 col-sm-12 col-lg-4 col-md-4 ">
                    <div class="card border-0" style="width: 18rem;">
                        <img class="card-img-top" src="{{ asset('frontend-assets/image/viktor/v3.png') }}" alt="">
                        <div class="card-body">
                          <p class="card-text">Privasi anda terjamin</p>
                        </div>
                      </div>
                    </div>
               </div>
           </div>
       </div>
    </div>
</section>