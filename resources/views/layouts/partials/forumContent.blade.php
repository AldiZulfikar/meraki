<section class="artikel__content mx-5">
    <div class="container">
        @foreach($forum as $frm)
        <div class="card " style="width: 100%; margin-top:2rem">
            <div class="card-body">
                    <span class="d-flex">
                    <div class="logo mr-3 mt-n1">
                        <img src="{{asset('/frontend-assets/image/logo/logo2.png')}}" alt="">
                    </div>
                        <p class="card-text">Posted by {{$frm -> users-> name}} 10 hours ago</p>
                    </span>
                    <h3 class="card-title mt-2">{{$frm -> judul}}</h3>
                    <p class="card-text">{{$frm -> konten}}</p>
                    <div class="action mt-n3 ml-n2">
                        <button class="btn btn-komentar" >komentar</button>
                        <button class="btn ">Suka</button>
                        <button class="btn ">Report</button>
                    </div>
                    <div id="comment">
                            @foreach($frm -> komentar as $frmk)
                            <div class="card bg-white mx-3" style="width: 100%; margin-top:2rem">
                                <div class="card-body">
                                        <span class="d-flex">
                                        <div class="logo mr-3 mt-n1">
                                            <img src="{{asset('/frontend-assets/image/logo/logo2.png')}}" alt="">
                                        </div>
                                            <p class="card-text">Posted by {{$frmk -> users-> name}} 10 hours ago</p>
                                        </span>
                                        <h3 class="card-title">Judul ga ada</h3>
                                        <p class="card-text">{{$frmk -> konten}}</p>
                                        <button class="btn btn-komentar" >komentar</button>
                                        <button class="btn ">Suka</button>
                                        <button class="btn ">Report</button>
                                    </div>
                            </div>
                    @endforeach
                    <form action="" method="post" class="ml-4 ">
                        @csrf
                            <input type="hidden" name="forum_id" value="{{$frm -> id}}">
                            <input type="hidden" name="slug" value="{{$frm -> slug}}">
                            <textarea style=" margin-top:10px;" name="konten" id="komentar" class="form-control" cols="30" rows="4"></textarea>
                            <button class="btn btn-primary mt-4" type="submit">kirim</button>
                        </form>
                        </div>
                      
                    </div>
                    
    
                   
                </div>
            </div>
        </div>
        @endforeach

    </div>
</section>
