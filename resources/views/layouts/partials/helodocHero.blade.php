<section class="adopsi__home d-flex align-items-center position-relative" >
    <div class="container  ">
    <div class="hello__banner">
        <img src="{{ asset('frontend-assets/image/banner/hallo1.png') }}" alt="">
    </div>
    <div class="hello__banner2">
        <img src="{{ asset('frontend-assets/image/banner/hello2.png') }}" alt="">
    </div>
    <div class="row d-flex ml-3">
        <div class="col-lg-5 col-md-7 col-sm-1 2 mt-3">
            <div class="desc position-relative mt-5 pt-5">
                <img class="position-absolute mt-5" src="{{ asset('frontend-assets/image/icon/kakikucing.png') }}" alt="">
                <h1>
                    Solusi kesehatan terbaik 
                </h1>
                <p >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc tellus quam, convallis et efficitur sit amet, condimentum sed neque. Ut ornare diam dapibus volutpat auctor. Proin ultrices vehicula mauris, nec tincidunt dolor efficitur in. assa congue dictum vitae vel metus. </p>
                <button class="btn btn-primary">
                    Hubungi Dokter
                </button>
            </div>
        </div>
        <div class="col-lg-4 col-md-5 col-sm-12">
            <div class="adopsi-image">
                <img src="{{ asset('frontend-assets/image/viktor/hello.png') }}" alt="">
            </div>
        </div>
    </div>
</div>
</section>
