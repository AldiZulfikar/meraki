<section class="homepage__banner row position-relative">
    <div class="container ml-5 overflow-hidden">
        <div class="homepage__desc col-md-8 col-lg-5 col-sm-12 ml-4">
            <h3>Selamatkan Kucing</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec aliquet scelerisque porta. Nunc dignissim egestas sem, non facilisis mauris tempor et. Aliquam ut dui </p>
            <button class="btn btn-primary">Mulai</button>
        </div>
        <div class="homepage__background">
            <img src="{{asset('/frontend-assets/image/banner/homepage.png')}}" alt="">
        </div>
        <div class="homepage__ornamen">
            <img src="{{asset('/frontend-assets/image/viktor/ornamen-besar.png')}}" alt="">
        </div>
        <div class="homepage__ornamen2">
            <img src="{{asset('/frontend-assets/image/viktor/ornamen-kecil.png')}}" alt="">
        </div>
        <div class="homepage__cat d-none d-lg-block d-xl-block">
            <img src="{{asset('/frontend-assets/image/viktor/cat-hero.png')}}" alt="">
        </div>
    </div>
</section>