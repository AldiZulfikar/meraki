<section class="adopsi__daftar d-flex align-items-center justify-content-center">
    <div class="container">

        {{-- nanti kamu looping ya datanya --}}
        <div class="row ml-5 mb-5">
          @foreach($adopsi as $data)
            <div class="col-4 col-sm-12 col-lg-4 col-md-4">  
                <div class="card" style="width: 18rem;">
                <img class="card-img-top"  src="{{ asset('frontend-assets/image/foto/adopsi.jpg') }}" alt="">
                <div class="card-body">
                  <h5 class="card-title">{{$data -> judul}}</h5>
                  <p class="card-text">{{$data -> alamat}}</p>
                  <a href="/adopsi/detail/{{$data->slug}}" class="btn btn-primary">Go somewhere</a>
                </div>
              </div>
            </div>
          @endforeach
        </div>
    </div>
  
</section>