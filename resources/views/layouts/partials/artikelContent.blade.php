<section class="artikel__content ml-5">
    <a href="/Homepage" class="btn btn-artikel"> <i class="fas fa-angle-left"></i><span class="ml-2">Back</span> </a>
    <div class="col-10 text-center m-auto mb-5">
        <h1 class="fw-bold mt-5">{{$artikel->judul}}</h1>
        <p>28 Oktober 2021</p>
        <p  class="text-start">{{$artikel->konten}}</p>
        <div class="artikel-img mb-3">
            <img src="{{ asset('frontend-assets/image/foto/detail.jpg') }}" alt="">
        </div>
        <p  class="text-start">{{$artikel->konten}}</p>
    </div> 
</section>
