<div class="sidebar ">
    <div class=" sidebar__section-1 position-fixed d-flex justify-content-center flex-column pl-5">
            <div class="alamat ">
                <h3 >Alamat <span>(Bisa diganti apa aja nanti) </span></h3>
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing 
                    elit, sed do eiusmod tempor incididunt ut labore et 
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud”</p>
            </div>
            <div class="contact d-flex justify-content-between  ">
                <div class="telepon">
                    <p class="mb-0">Telepon</p>
                    <a href="" class="text-white">021-2345-6789</a>
                </div>
                <div class="email">
                    <p class="mb-0">Email</p>
                    <a class="text-white" href="">pethero@gmail.com</a>
                </div>
            </div>
            <div class="sosmed d-flex mt-3">
                <div class="icon facebook">
                    <i class="fab fa-facebook-f"></i>
                </div>
                <div class="icon instagram">
                    <i class="fab fa-instagram"></i>
                </div>
                <div class="icon twitter">
                    <i class="fab fa-twitter"></i>
                </div>
                <div class="icon youtube">
                    <i class="fab fa-youtube"></i>
                </div>
                <div class="icon linked">
                    <i class="fab fa-linkedin"></i>
                </div>
            </div>
    </div>
    <div class="sidebar__section-2 position-fixed flex-row-reverse">
        <i class="close fas fa-times"></i>
        <nav class="justify-content-end d-flex m-5">
            <ul class="list-unstyled">
                <li><img src="{{asset('/frontend-assets/image/logo/logo.png')}}" alt=""></li>
                @guest
                    @if (Route::has('login'))
                        <li ><a href="{{ route('login') }}" style="text-decoration:none"><p>{{ __('Login | Register') }}</p></a></li>
                    @endif
                @else
                    <li ><p>{{ Auth::user()->name }}</p></li>
                @endguest
                <li><a class="text-white" href="{{route('pages.homepage')}}">Home</a></li>
                <li><a href="{{route('baca-artikel')}}" class="text-white">Baca Artikel</a></li>
                <li><a href="{{route('pages.donasi')}}" class="text-white">Mari Berdonasi</a></li>
                <li><a href="{{route('forum-diskusi')}}" class="text-white ">Forum Komunitas</a></li>
            </ul>
        </nav>
    </div>
</div>