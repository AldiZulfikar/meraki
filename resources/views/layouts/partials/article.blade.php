<section class="artikel d-flex align-items-center">
    <div class="artikel-swiper swiper-container col-lg-10">
        <h1 class="fw-bold text-center my-5">Artikel</h1>
        <div class="swiper-wrapper">
           {{-- nanti loopingin yaa makasi make data yg didatabase --}}
            @foreach($artikel as $data)
            <div class="swiper-slide d-flex ">
                  <div class="image col-10 col-sm-10 col-md-6 col-lg-6" style="background-image: url('frontend-assets/image/banner/artikel-1.png');">
                  </div>
                  <div class="desc col-10 col-sm-10 col-md-6 col-lg-6">
                  <a href="/baca-artikel/{{$data->slug}}" style="text-decoration:none">
                     <h3 class="text-center mt-1">{{$data->judul}}</h3>
                     <p class=" mt-1">{{$data->konten}}</p>
                  </a>
                  </div>
            </div>
            @endforeach
         </div>
     <div class="swiper-pagination ">
       
   </div>
   </div>
{{-- 
    <p>{{$artikel->judul}}</p>
    <p>{{$artikel->kategori->nama_kategori }}</p>
    <p>{{$artikel->gambar}}</p>
    <p>{{$artikel->created_at}}</p> --}}
   </section>
