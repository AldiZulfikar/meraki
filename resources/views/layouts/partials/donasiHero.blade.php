<section class="donasi__home d-flex align-items-center" >
<div class="container ">
    <div class="row d-flex justify-content-center ml-3 ">
        <div class="col-lg-7 col-md-7 col-sm-12">
            <div class="desc position-relative mt-5">
                <img class="position-absolute" src="{{ asset('frontend-assets/image/icon/kakikucing.png') }}" alt="">
                <h1>
                    Bantu para kucing dengan berikan donasi anda
                </h1>
                <p >Lorem ipsum Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores reiciendis tempora deleniti provident corporis, blanditiis quasi architecto nobis officiis obcaecati sint hic nostrum fugiat nulla temporibus reprehenderit ullam accusantium impedit. dolor sit amet, consectetur adipiscing elit. Nunc tellus quam, convallis et efficitur sit amet, condimentum sed neque. Ut ornare diam dapibus volutpat auctor. Proin ultrices vehicula mauris, nec tincidunt dolor efficitur in. assa congue dictum vitae vel metus. </p>
                <button class="btn btn-primary">
                    Mulai Donasi
                </button>
            </div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-12">
            <div class="adops-image">
                <img src="{{ asset('frontend-assets/image/viktor/donasi.png') }}" alt="">
            </div>
        </div>
    </div>
</div>
</section>
