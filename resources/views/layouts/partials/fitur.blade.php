<div class="fitur d-flex align-items-center justify-content-center">
    <div class="row col-10 d-flex align-items-center justify-content-center py-4 my-5 ">
        <h1 class="text-center mb-5 fw-bold mt-3">Fitur Terbaru</h1>
        <div class="col-lg-4 col-md-12 col-sm-12 p-0 justify-content-center d-flex ">
          <a href="{{route('pages.adopsi')}}" style="text-decoration:none">
            <div class="card" style="width: 17rem;">
                <img src="{{asset('/frontend-assets/image/icon/cakar.png')}}" alt="">
                <div class="card-body text-center">
                  <h5 class="card-title  fw-bold">Adopsi</h5>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                </div>
              </div>
          </a>
        </div>
        <div class="col-lg-4 col-md-12 col-sm-12 p-0 justify-content-center d-flex ">
          <a href="{{route('pages.helodoc')}}" style="text-decoration:none">
            <div class="card" style="width: 17rem;">
                <img src="{{asset('/frontend-assets/image/icon/cakar.png')}}" alt="">
                <div class="card-body text-center">
                  <h5 class="card-title  fw-bold">Konsultasi Dokter</h5>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                </div>
              </div>
          </a>
        </div>
        <div class="col-lg-4 col-md-12 col-sm-12 p-0 justify-content-center d-flex">
          <a href="{{route('salon-hewan')}}" style="text-decoration:none">
            <div class="card" style="width: 17rem;">
                <img src="{{asset('/frontend-assets/image/icon/cakar.png')}}" alt="">
                <div class="card-body text-center">
                  <h5 class="card-title  fw-bold">Salon Hewan</h5>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                </div>
              </div>
          </a>
        </div>
    </div>
</div>