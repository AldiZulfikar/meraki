<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <script type="text/javascript" src="{{asset('/frontend-assets/js/jquery.min.js')}}"></script>
    <!-- Styles -->
    <link href="{{ asset('frontend-assets/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend-assets/css/app.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('frontend-assets/css/fontawesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('/frontend-assets/css/swiper.min.css')}}" />


</head>
<body>
    <div id="app" >
        <main>
            @yield('content')
            <a class="dropdown-item" href="{{ route('logout') }}"
              onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </main>
    </div>
    <script type="text/javascript" src="{{asset('/frontend-assets/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/frontend-assets/js/swiper.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/frontend-assets/js/custom.js')}}"></script>
   <script>
         $('.nav-signup').click( function(){ 
             console.log("oke")
            $('.nav-sigin').removeClass('active')
            $('.nav-signup').addClass('active')
            $('.container-signup').addClass('active')
            $('.container-signin').removeClass('active')
       });
         $('.nav-sigin').click( function(){ 
             console.log("oke")
            $('.nav-signup').removeClass('active')
            $('.nav-sigin').addClass('active')
            $('.container-signup').removeClass('active')
            $('.container-signin').addClass('active')
       });
    </script>   
</body>
</html>
