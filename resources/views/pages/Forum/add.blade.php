@extends('layouts.partials.body')
@section('content')

<section class="forumAdd">
    <br><br><br>
  <div class="row">
      <div class="col-3"></div>
      <div class="col-6">
        <form method="post" action="{{route('forum-buat-diskusi')}}" enctype="multipart/form-data">
        @csrf
            <div class="form-group">
                <input type="text" name="judul" class="form-control" id="exampleFormControlInput1" placeholder="Judul Forum">
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Apa yang anda Pikirkan?</label>
                <textarea class="form-control" name="konten" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
            <button type="submit" class="btn btn-primary mb-2">Kirim</button>
        </form>
    </div>
 </div>
</section>

@endsection