@extends('layouts.partials.body')
@section('content')

<section class="forumIndex">
    @include('layouts.partials.forumHero')
    @include('layouts.partials.forumHeader')
    @include('layouts.partials.forumContent')
    <div class="footer mt-5">
        @include('layouts.partials.footer')
    </div>

</section>

@endsection