@extends('layouts.partials.body')
@section('content')

<section class="adopsi "style="background-image: url('frontend-assets/image/banner/Adopsi.png');">
    @include('layouts.partials.adopsiHero')
    @include('layouts.partials.daftarAdopsi')
    @include('layouts.partials.footer')
</section>

@endsection