@extends('layouts.partials.body')
@section('content')

<section>
<br><br><br>
  <div class="row">
      <div class="col-3"></div>
      <div class="col-6">
        <form method="post" action="{{route('buat-adopsi')}}" enctype="multipart/form-data">
        @csrf
            <div class="form-group">
                <input type="text" name="judul" class="form-control" id="exampleFormControlInput1" placeholder="Judul adopsi">
            </div>
            <div class="form-group">
                <input type="text" name="alamat" class="form-control" id="exampleFormControlInput1" placeholder="alamat">
            </div>
            <div class="form-group">
                <input type="text" name="ras" class="form-control" id="exampleFormControlInput1" placeholder="ras binatang">
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Keterangan</label>
                <textarea class="form-control" name="konten" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
            <div class="form-group">
                <input type="file" name="gambar_binatang" class="form-control" id="exampleFormControlInput1">
            </div>
            <div class="form-group">
                <input type="file" name="persyaratan" class="form-control" id="exampleFormControlInput1">
            </div>
            <button type="submit" class="btn btn-primary mb-2">Kirim</button>
        </form>
    </div>
 </div>
</section>

@endsection