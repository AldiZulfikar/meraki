@extends('layouts.partials.body')
@section('content')

<section class="hello">
    @include('layouts.partials.helodocHero')
    @include('layouts.partials.heloContent')
    @include('layouts.partials.footer')
</section>

@endsection