@extends('layouts.partials.body')
@section('content')

<section>
    @include('pages.salon.partials.headerSalon')
    @include('pages.salon.partials.contentSalon')

    <div class="footer mt-5">
        @include('layouts.partials.footer')
    </div>

</section>

@endsection