<footer class="footer footer-klinik" >
    <div class="banner d-flex justify-content-between">
        <div class="mascot ml-5">
            <img src="{{ asset('frontend-assets/image/viktor/maskot.png') }}" alt="">
        </div>
        <div class="kontak col-4 align-self-end text-end mr-5">
            <div class="alamat ">
                <p class="fw-bold">Alamat</p>
                <p class="mt-n3">"Lorem ipsum dolor sit amet, consectetur adipiscing 
                    elit, sed do eiusmod tempor incididunt ut labore et 
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud”</p>

            </div>
            <div class="contact d-flex flex-row-reverse justify-content-between ml-auto ">
                <div class="telepon ">
                    <p class="mb-0">Telepon</p>
                    <a href="" class="text-white">021-2345-6789</a>
                </div>
                <div class="email">
                    <p class="mb-0">Email</p>
                    <a class="text-white" href="">cathero@gmail.com</a>
                </div>
            </div>
            <div class="sosmed d-flex justify-content-between mt-3">
                <div class="icon facebook">
                    <i class="fab fa-facebook-f ml-3"></i>
                </div>
                <div class="icon instagram">
                    <i class="fab fa-instagram"></i>
                </div>
                <div class="icon twitter">
                    <i class="fab fa-twitter"></i>
                </div>
                <div class="icon youtube">
                    <i class="fab fa-youtube"></i>
                </div>
                <div class="icon linked ">
                    <i class="fab fa-linkedin"></i>
                </div>
            </div>

        </div>
    </div>
</footer>