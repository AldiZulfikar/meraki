<section class="salon__content d-flex mt-4">
    <div class="salon__sidebar col-2 col-md-2 col-lg-2 col-sm-12">
        @include('pages.salon.partials.sidebarSalon')
    </div>
    <div class="isi_salon col-10 col-md-11 col-lg-10 col-sm-12 ">
        <div class="nav-content d-flex align-items-center">
           <button class="btn  ">
            Salon Hewan
           </button>
           <div class="vr">l</div>
           <button class="btn fw-bold">
            Klinik Hewan
           </button>
        </div>
        <div class="d-flex box-table bg-cream justify-content-between p-3" >
            {{-- Looping isi tablenya ya --}}
            <div class="card col-6 col-sm-6 col-lg-3 col-md-6 border-cream p-0 border-4 m-1 mx-1" style="width: 15rem;">
                <img class="card-img-top" src="{{ asset('frontend-assets/image/foto/salon.png') }}" alt="">
                <div class="card-body">
                  <p class=" fw-bold mb-1">Makmur Jaya Sentosa Pet Salon and grooming</h5>
                    <div class="locate d-flex align-items-center">
                        <i class="fas fa-map-marker-alt mr-2 color-gray"></i>
                        <p class="mb-0 card-text">Ciputat, Tanggerang Selatan  2.7Km</p>

                    </div>
                  <div class="rate d-flex align-items-center mt-2 mb-2">
                      <div class="star d-flex">
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                      </div>
                      <p class="card-text">(45)</p>
                  </div>
                  <div class="bisa d-flex align-items-center justify-content-between">
                      <p class="mb-0 card-text ">Bisa antar Jemput/ Datang Kerumah</p>
                      <i class="far fa-check-circle color-primary"></i>
                  </div>
                  <div class="box d-flex  align-items-center justify-content-between">
                      <a href="#" class="btn btn-primary">Detail Lebih Lanjut</a>
                      <i class="fas fa-heart color-primary"></i>
                  </div>
                </div>
            </div>
            <div class="card col-6 col-sm-6 col-lg-3 col-md-6 border-cream p-0 border-4 m-1" style="width: 15rem;">
                <img class="card-img-top" src="{{ asset('frontend-assets/image/foto/salon.png') }}" alt="">
                <div class="card-body">
                  <p class=" fw-bold mb-1">Makmur Jaya Sentosa Pet Salon and grooming</h5>
                    <div class="locate d-flex align-items-center">
                        <i class="fas fa-map-marker-alt mr-2 color-gray"></i>
                        <p class="mb-0 card-text">Ciputat, Tanggerang Selatan  2.7Km</p>

                    </div>
                  <div class="rate d-flex align-items-center mt-2 mb-2">
                      <div class="star d-flex">
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                      </div>
                      <p class="card-text">(45)</p>
                  </div>
                  <div class="bisa d-flex align-items-center justify-content-between">
                      <p class="mb-0 card-text ">Bisa antar Jemput/ Datang Kerumah</p>
                      <i class="far fa-check-circle color-primary"></i>
                  </div>
                  <div class="box d-flex  align-items-center justify-content-between">
                      <a href="#" class="btn btn-primary">Detail Lebih Lanjut</a>
                      <i class="fas fa-heart color-primary"></i>
                  </div>
                </div>
            </div>
            <div class="card col-6 col-sm-6 col-lg-3 col-md-6 border-cream p-0 border-4 m-1" style="width: 15rem;">
                <img class="card-img-top" src="{{ asset('frontend-assets/image/foto/salon.png') }}" alt="">
                <div class="card-body">
                  <p class=" fw-bold mb-1">Makmur Jaya Sentosa Pet Salon and grooming</h5>
                    <div class="locate d-flex align-items-center">
                        <i class="fas fa-map-marker-alt mr-2 color-gray"></i>
                        <p class="mb-0 card-text">Ciputat, Tanggerang Selatan  2.7Km</p>

                    </div>
                  <div class="rate d-flex align-items-center mt-2 mb-2">
                      <div class="star d-flex">
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                      </div>
                      <p class="card-text">(45)</p>
                  </div>
                  <div class="bisa d-flex align-items-center justify-content-between">
                      <p class="mb-0 card-text ">Bisa antar Jemput/ Datang Kerumah</p>
                      <i class="far fa-check-circle color-primary"></i>
                  </div>
                  <div class="box d-flex  align-items-center justify-content-between">
                      <a href="#" class="btn btn-primary">Detail Lebih Lanjut</a>
                      <i class="fas fa-heart color-primary"></i>
                  </div>
                </div>
            </div>
            <div class="card col-6 col-sm-6 col-lg-3 col-md-6 border-cream p-0 border-4 m-1" style="width: 15rem;">
                <img class="card-img-top" src="{{ asset('frontend-assets/image/foto/salon.png') }}" alt="">
                <div class="card-body">
                  <p class=" fw-bold mb-1">Makmur Jaya Sentosa Pet Salon and grooming</h5>
                    <div class="locate d-flex align-items-center">
                        <i class="fas fa-map-marker-alt mr-2 color-gray"></i>
                        <p class="mb-0 card-text">Ciputat, Tanggerang Selatan  2.7Km</p>

                    </div>
                  <div class="rate d-flex align-items-center mt-2 mb-2">
                      <div class="star d-flex">
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                      </div>
                      <p class="card-text">(45)</p>
                  </div>
                  <div class="bisa d-flex align-items-center justify-content-between">
                      <p class="mb-0 card-text ">Bisa antar Jemput/ Datang Kerumah</p>
                      <i class="far fa-check-circle color-primary"></i>
                  </div>
                  <div class="box d-flex  align-items-center justify-content-between">
                      <a href="#" class="btn btn-primary">Detail Lebih Lanjut</a>
                      <i class="fas fa-heart color-primary"></i>
                  </div>
                </div>
            </div>
           
        </div>
        <div class="d-flex box-table bg-cream justify-content-between p-3">
            {{-- Looping isi tablenya ya --}}
            <div class="card col-6 col-sm-6 col-lg-3 col-md-6 border-cream p-0 border-4 m-1 mx-1" style="width: 15rem;">
                <img class="card-img-top" src="{{ asset('frontend-assets/image/foto/salon.png') }}" alt="">
                <div class="card-body">
                  <p class=" fw-bold mb-1">Makmur Jaya Sentosa Pet Salon and grooming</h5>
                    <div class="locate d-flex align-items-center">
                        <i class="fas fa-map-marker-alt mr-2 color-gray"></i>
                        <p class="mb-0 card-text">Ciputat, Tanggerang Selatan  2.7Km</p>

                    </div>
                  <div class="rate d-flex align-items-center mt-2 mb-2">
                      <div class="star d-flex">
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                      </div>
                      <p class="card-text">(45)</p>
                  </div>
                  <div class="bisa d-flex align-items-center justify-content-between">
                      <p class="mb-0 card-text ">Bisa antar Jemput/ Datang Kerumah</p>
                      <i class="far fa-check-circle color-primary"></i>
                  </div>
                  <div class="box d-flex  align-items-center justify-content-between">
                      <a href="#" class="btn btn-primary">Detail Lebih Lanjut</a>
                      <i class="fas fa-heart color-primary"></i>
                  </div>
                </div>
            </div>
            <div class="card col-6 col-sm-6 col-lg-3 col-md-6 border-cream p-0 border-4 m-1" style="width: 15rem;">
                <img class="card-img-top" src="{{ asset('frontend-assets/image/foto/salon.png') }}" alt="">
                <div class="card-body">
                  <p class=" fw-bold mb-1">Makmur Jaya Sentosa Pet Salon and grooming</h5>
                    <div class="locate d-flex align-items-center">
                        <i class="fas fa-map-marker-alt mr-2 color-gray"></i>
                        <p class="mb-0 card-text">Ciputat, Tanggerang Selatan  2.7Km</p>

                    </div>
                  <div class="rate d-flex align-items-center mt-2 mb-2">
                      <div class="star d-flex">
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                      </div>
                      <p class="card-text">(45)</p>
                  </div>
                  <div class="bisa d-flex align-items-center justify-content-between">
                      <p class="mb-0 card-text ">Bisa antar Jemput/ Datang Kerumah</p>
                      <i class="far fa-check-circle color-primary"></i>
                  </div>
                  <div class="box d-flex  align-items-center justify-content-between">
                      <a href="#" class="btn btn-primary">Detail Lebih Lanjut</a>
                      <i class="fas fa-heart color-primary"></i>
                  </div>
                </div>
            </div>
            <div class="card col-6 col-sm-6 col-lg-3 col-md-6 border-cream p-0 border-4 m-1" style="width: 15rem;">
                <img class="card-img-top" src="{{ asset('frontend-assets/image/foto/salon.png') }}" alt="">
                <div class="card-body">
                  <p class=" fw-bold mb-1">Makmur Jaya Sentosa Pet Salon and grooming</h5>
                    <div class="locate d-flex align-items-center">
                        <i class="fas fa-map-marker-alt mr-2 color-gray"></i>
                        <p class="mb-0 card-text">Ciputat, Tanggerang Selatan  2.7Km</p>

                    </div>
                  <div class="rate d-flex align-items-center mt-2 mb-2">
                      <div class="star d-flex">
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                      </div>
                      <p class="card-text">(45)</p>
                  </div>
                  <div class="bisa d-flex align-items-center justify-content-between">
                      <p class="mb-0 card-text ">Bisa antar Jemput/ Datang Kerumah</p>
                      <i class="far fa-check-circle color-primary"></i>
                  </div>
                  <div class="box d-flex  align-items-center justify-content-between">
                      <a href="#" class="btn btn-primary">Detail Lebih Lanjut</a>
                      <i class="fas fa-heart color-primary"></i>
                  </div>
                </div>
            </div>
            <div class="card col-6 col-sm-6 col-lg-3 col-md-6 border-cream p-0 border-4 m-1" style="width: 15rem;">
                <img class="card-img-top" src="{{ asset('frontend-assets/image/foto/salon.png') }}" alt="">
                <div class="card-body">
                  <p class=" fw-bold mb-1">Makmur Jaya Sentosa Pet Salon and grooming</h5>
                    <div class="locate d-flex align-items-center">
                        <i class="fas fa-map-marker-alt mr-2 color-gray"></i>
                        <p class="mb-0 card-text">Ciputat, Tanggerang Selatan  2.7Km</p>

                    </div>
                  <div class="rate d-flex align-items-center mt-2 mb-2">
                      <div class="star d-flex">
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                          <i class="fas fa-star"></i>
                      </div>
                      <p class="card-text">(45)</p>
                  </div>
                  <div class="bisa d-flex align-items-center justify-content-between">
                      <p class="mb-0 card-text ">Bisa antar Jemput/ Datang Kerumah</p>
                      <i class="far fa-check-circle color-primary"></i>
                  </div>
                  <div class="box d-flex  align-items-center justify-content-between">
                      <a href="#" class="btn btn-primary">Detail Lebih Lanjut</a>
                      <i class="fas fa-heart color-primary"></i>
                  </div>
                </div>
            </div>
        </div>
    </div>

</section>