@extends('layouts.partials.body')
@section('content')

<div class="homepage overflow-hidden">
    @include('layouts.partials.hero')
    @include('layouts.partials.fitur')
    @include('layouts.partials.article')
    @include('layouts.partials.footer')

</div>
@endsection