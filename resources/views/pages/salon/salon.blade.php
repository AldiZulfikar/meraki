@extends('layouts.partials.body')
@section('content')

<section class="overflow-hidden">
    @include('pages.salon.partials.headerSalon')
    @include('pages.salon.partials.contentSalon')
    
    <div class="footer mt-5">
        @include('pages.salon.partials.footerKlinik')

    </div>

</section>

@endsection