@extends('layouts.partials.body')
@section('content')

<section class="donasi overflow-hidden "style="background-image: url('frontend-assets/image/banner/Adopsi.png');">
    @include('layouts.partials.donasiHero')
    @include('layouts.partials.daftarDonasi')
    @include('layouts.partials.footer')
</section>

@endsection